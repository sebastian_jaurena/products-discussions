<?php
class CommentsController extends AppController 
{
  var $uses = array('Comment', 'User');

  function beforeFilter()
  {
    if($this->Session->check('user') == false)
    {
      $this->redirect(array('controller' => 'users', 'action' => 'login'));
    }
  }
  
  function add_comment()
  {
    $this->layout = 'ajax';
    
    if(!empty($this->data)) 
    {
      $this->Comment->create();
      $comment = $this->Comment->save($this->data);
      
      if($comment)
      {
        $user = $this->Session->read('user');
        
        $this->set('comment', $comment['Comment']['text']);
        $this->set('username', $user['username']);
      }
    }
  }
}