<?php
class ProductsController extends AppController 
{
  var $uses = array('Product', 'Comment', 'User');
  var $helpers = array('Javascript', 'Text');

  function beforeFilter()
  {
    if($this->Session->check('user') == false)
    {
      $this->redirect(array('controller' => 'users', 'action' => 'login'));
    }
  }
  
  function index()
  {
    $this->set('products', $this->Product->find('all'));
    
    $user = $this->Session->read('user');
    $this->set('admin', $user['admin']);
  }
  
  function get_product($id = null)
  {
    $this->layout = 'ajax';
    $product = $this->Product->find('first', array(
      'conditions' => array(
        'id' => $id
      ),
      'recursive' => 2
    ));
    $user = $this->Session->read('user');
    
    $this->set('product', $product);
    $this->set('user_id', $user['id']);
    $this->set('admin', $user['admin']);
  }
  
  function close_comments($id)
  {
    $closed = false;
    
    $this->layout = 'ajax';
    
    $user = $this->Session->read('user');
    
    if($user['admin'])
    {
      $this->Product->id = $id;
      $this->Product->saveField('closed', '1');
      $closed = true;
    }
    
    $this->set('closed', $closed);
  }
}