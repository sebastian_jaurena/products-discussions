<?php  
class UsersController extends AppController 
{ 
  var $name = 'Users'; 
  var $helpers = array('Html', 'Form'); 
      
  function beforeFilter() 
  { 
    if(($this->Session->check('user') != false) && ($this->action != 'logout')) 
    { 
      $this->redirect(array('controller' => 'products', 'action' => 'index')); 
    }
  } 
   
  function login() 
  { 
    if(empty($this->data) == false) 
    { 
      if(($user = $this->User->validateLogin($this->data['User'])) == true) 
      { 
        $this->Session->write('user', $user);  
        $this->redirect(array('controller' => 'products', 'action' => 'index')); 
        exit();
      } 
    } 
  } 
   
  function logout() 
  { 
    $this->Session->destroy('User'); 
    $this->Session->setFlash('You\'ve successfully logged out.'); 
    $this->redirect('login'); 
  }
} 