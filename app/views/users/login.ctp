<div class="row">
  <div class="col-lg-4 col-lg-offset-4">
    <div class="well">   
      <?php echo $form->create('User', array('action' => 'login', 'class' => 'form-horizontal'));?>
        <fieldset> 
          <legend>Login</legend>
          <div class="form-group">
            <?php echo $form->label('username', 'Username', array('class' => 'col-lg-3 control-label')); ?>
            <div class="col-lg-9">
              <?php echo $form->text('username', array('class' => 'form-control', 'placeholder' => 'Your username...')); ?>
            </div>
          </div>
          <div class="form-group">
            <?php echo $form->label('password', 'Password', array('class' => 'col-lg-3 control-label')); ?>
            <div class="col-lg-9">
              <?php echo $form->password('password', array('class' => 'form-control', 'placeholder' => '********')); ?>
            </div>
          </div>
          <div class="form-group">
            <div class="col-lg-9 col-lg-offset-3">
              <?php echo $form->button('Login', array('type'=>'submit', 'class' => 'btn btn-primary')); ?>
            </div>
          </div> 
        </fieldset>
      <?php echo $form->end(); ?> 
    </div>
  </div>
</div>