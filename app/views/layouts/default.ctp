<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Products</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <!-- 
    <link rel="stylesheet" href="./bootstrap.css" media="screen">
    <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../assets/css/bootswatch.min.css">
     -->
    <?php echo $html->css('bootstrap'); ?>
    <?php echo $html->css('font-awesome.min'); ?>
    <?php echo $html->css('bootswatch.min'); ?>
    <?php echo $html->css('products'); ?>
  </head>
  <body>
    <div class="navbar navbar-default">
      <div class="navbar-header">
        <a class="navbar-brand" href="#"><?php echo $html->image('logo.png');?></a>
      </div>
      <div class="navbar-collapse collapse navbar-responsive-collapse">
        <?php if($session->check('user')): ?>
        <ul class="nav navbar-nav navbar-right">
          <li><?php echo $html->link('Logout', array('controller' => 'users', 'action' => 'logout')); ?></li>
        </ul>
        <?php endif; ?>
      </div>
    </div>
    <div class="container">
      <?php echo $content_for_layout; ?>
    </div>
    <?php if(isset($javascript)): ?>
    <?php echo $javascript->link('jquery.min'); ?>
    <?php echo $javascript->link('bootstrap.min'); ?>
    <?php echo $javascript->link('products'); ?>
    <?php endif; ?>
  </body>
</html>