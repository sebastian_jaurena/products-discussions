<div class="panel panel-default">
  <div class="panel-body">
    <h3><?php echo $product['Product']['name']; ?></h3>
    <div>
      <div class="pull-left product-thumb">
        <img src="<?php echo $product['Product']['photo']; ?>" class="img-thumbnail">
      </div>
      <p>
        <?php echo $product['Product']['description']; ?> 
      </p>
    </div>
  </div>
</div>
<div id="comments">
  <?php foreach($product['Comment'] as $comment): ?>
  <blockquote>
    <p><?php echo $comment['text']; ?></p>
    <small><?php echo $comment['User']['username']?></small>
  </blockquote>
  <?php endforeach; ?>
</div>
<?php if($product['Product']['closed']): ?>
<div class="alert alert-danger">
  <p>Comments have been disabled by the administrator.</p>
</div>
<?php else: ?>
<div class="well">
  <div class="add-comment">
    <?php echo $form->create('Comment', array('action' => 'add_comment', 'class' => 'form-horizontal add-comment-form'));?>
      <?php echo $form->hidden('product_id', array('value' => $product['Product']['id'])); ?>
      <?php echo $form->hidden('user_id', array('value' => $user_id)); ?>
      <fieldset> 
        <div class="form-group">
          <?php echo $form->textarea('text', array('class' => 'form-control', 'placeholder' => 'Your comment...')); ?>
        </div>
        <div class="form-group">
          <?php echo $form->button('Add your comment!', array('type'=>'submit', 'class' => 'btn btn-primary')); ?>
          <?php if($admin): ?>
          <?php echo $html->link('Close comments?', array('controller' => 'products', 'action' => 'close_comments', 'id' => $product['Product']['id']), array('class' => 'btn btn-danger close-comments')); ?>
          <?php endif; ?>
        </div> 
      </fieldset>
    <?php echo $form->end(); ?>
  </div> 
</div>
<?php endif; ?>