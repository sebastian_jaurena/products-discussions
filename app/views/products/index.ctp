<?php if($admin): ?>
<div class="modal fade" id="add-product-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <h3 class="text-center">Sorry, this feature is not implemented!</h3>
      </div>
    </div>
  </div>
</div>
<?php endif; ?>
<div class="row">
  <div class="col-lg-5" id="products">
    <div class="list-group">
      <?php foreach($products as $product): ?>
      <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'get_product', 'id' => $product['Product']['id'])); ?>" class="list-group-item get-product">
        <h4 class="list-group-item-heading"><?php echo $product['Product']['name']; ?></h4>
        <p class="list-group-item-text">
          <?php echo $text->truncate($product['Product']['description'], 100, '...', false); ?>
        </p>
      </a>
      <?php endforeach; ?>
    </div>
    <?php if($admin): ?>
    <button class="btn btn-primary btn-lg btn-block add-product">Add new product!</button>
    <?php endif; ?>
  </div>
  <div class="col-lg-7" id="product"></div>
</div>