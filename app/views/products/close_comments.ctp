<?php if($closed): ?>
<div class="alert alert-danger">
  <p>Comments have been disabled by the administrator.</p>
</div>
<?php endif; ?>