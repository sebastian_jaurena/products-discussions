<?php  
class Comment extends AppModel 
{ 
  var $name = 'Comment'; 
  
  var $belongsTo = array(
    'Product' => array(
      'className'  => 'Product',
      'foreignKey' => 'product_id'
    ),
    'User' => array(
      'className'  => 'User',
      'foreignKey' => 'user_id'
    )
  );
} 