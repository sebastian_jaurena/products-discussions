<?php  
class User extends AppModel 
{ 
  var $name = 'User'; 
  
  var $hasMany = array(
    'Product' => array(
        'className'  => 'Product',
        'foreignKey' => 'product_id'
    ),
    'Comment' => array(
      'className' => 'Comment',
      'foreignKey' => 'user_id'
    )
  );
   
  function validateLogin($data) 
  { 
    $user = $this->find(array('username' => $data['username'], 'password' => md5($data['password'])));
    if(empty($user) == false) 
      return $user['User']; 
    return false; 
  }
} 