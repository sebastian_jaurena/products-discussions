$(document).ready(function(){
  
  $(document).on('click', '.get-product', function(e){
    e.preventDefault();
    $('#product').load($(this).attr('href'));
  });
  
  $(document).on('submit', '.add-comment-form', function(e){
    e.preventDefault();
    var data = $(this).serialize();
    this.reset();
    $.post($(this).attr('action'), data, function(response){
      $('#comments').append(response);
    });
  });
  
  $(document).on('click', '.close-comments', function(e){
    e.preventDefault();
    $.post($(this).attr('href'), function(response){
      $('.add-comment').parent().remove();
      $('#product').append(response);
    });
  });
  
  $(document).on('click', '.add-product', function(e){
    $('#add-product-modal').modal('show');
  });
});