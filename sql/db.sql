CREATE TABLE users ( 
  id INTEGER(10) UNSIGNED NOT NULL AUTO_INCREMENT, 
  username VARCHAR(20) NOT NULL, 
  password VARCHAR(40) NOT NULL, 
  email VARCHAR(40) NULL,
  admin TINYINT(1) DEFAULT 0,
  created DATETIME NULL, 
  PRIMARY KEY(id) 
); 

INSERT INTO users (id, username, password, email, admin, created) VALUES ('1', 'admin', MD5('1234'), 'admin@domain.com', 1, NOW());
INSERT INTO users (id, username, password, email, admin, created) VALUES ('2', 'user', MD5('1234'), 'user@domain.com', 0, NOW());

CREATE TABLE products ( 
  id INTEGER(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  user_id INTEGER(10) UNSIGNED NOT NULL, 
  name VARCHAR(20) NOT NULL, 
  photo VARCHAR(255) NOT NULL, 
  description TEXT NOT NULL,
  closed TINYINT(1) DEFAULT 0,  
  created DATETIME NOT NULL, 
  PRIMARY KEY(id) 
);

ALTER TABLE products ADD CONSTRAINT products_users FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE;

INSERT INTO products (id, user_id, name, photo, description, closed, created) VALUES
(1, 1, 'Energy Drink', 'http://www.goldenrule.com/images/how-energy-drinks-affect-your-health.jpg', 'How many calories do you consume in a day? If you don''t count calories, keep track of the number of calories you drink on a daily basis. From juice to soda, what we drink can add a significant amount to our daily calorie intake. Energy drinks often have 100 calories or more per serving. And it''s not just calories that can take a toll. Additives like caffeine and sugar can affect your overall health as well. Take a look at how many energy drinks you have during a normal day and consider making healthier choices.', 0, '2013-09-29 00:00:00'),
(2, 1, 'Body shapers protein', 'http://www.activesportnutrition.es/img/p/202-399-large.jpg', 'A dose for all who want to stay slim and dynamic reduce fat. Because we know that through atkins diet much carbohydrates gets absorbed, making people plump. As a result people increase their protein intake to offset the fat gained. Fat is really reduced only in such a way. Newest scientific realizations led to a revolution in the evaluation of diets. In the USA and other western industrialized nations, people continue to grow thicker, although they consume a great quantity of many fat-reduced products. But fat reduction alone is not enough, obviously. It was proven that the reduction of carboydrates is much more important. One who wants to decrease the fat does so not by reducing the muscle mass and fluid but by burning the fat. But in addition, the body needs a combination of the three components that is much protein, a reasonable portion of good fat and above all less carbohydrates. For this special shake we use high-quality raw materials and 50 percent pure milk so that it confirms to the biological and qualitative value. The quantity of 250 ml ideally fits a mealtime during sport. By the stable and light doses, one can now nourish oneself easily and have a healthy dose of protein. Cappuccino flavour.', 0, '0000-00-00 00:00:00'),
(3, 1, 'Glucon-D Froot Shot', 'http://easymandi.com/10377-4547-large/glucon-d-froot-shot-fruit-drink-with-real-fruit-powdermixed-fruit.jpg', 'Glucon-D Froot Shot - Fruit Drink with Real Fruit Powder (Mixed Fruit) contains real fruit powder, Vitamin C which helps in iron absorption and helps develop body assistance, Calcium which provides strength to the bones and Glucose, the fastest energy source to the body.', 0, '0000-00-00 00:00:00');

CREATE TABLE comments ( 
  id INTEGER(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  product_id INTEGER(10) UNSIGNED NOT NULL,
  user_id INTEGER(10) UNSIGNED NOT NULL, 
  text TEXT NOT NULL, 
  created DATETIME NULL, 
  PRIMARY KEY(id) 
);

ALTER TABLE comments ADD CONSTRAINT comments_users FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE;
ALTER TABLE comments ADD CONSTRAINT comments_products FOREIGN KEY (product_id) REFERENCES products(id) ON DELETE CASCADE;